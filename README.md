# EMS

An Education Managment System written in one day.

It's one of my **Advanced Programming** course's projects at Shiraz University.

Please do not use this software in production.

## Clone it!

```
git clone git@gitlab.com:alifarazz/EMS.git
```

## Contribute!

Fork the project, create a new branch, add your changes, submit a merge request.
