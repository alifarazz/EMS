package Controller;

import Model.EducationalSystem.Student;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;

public class StudentProfileTableViewItem {
    private StringProperty key;
    private StringProperty value;

    private StudentProfileTableViewItem(String key, String value) {
        this.key = new SimpleStringProperty(key);
        this.value = new SimpleStringProperty(value);
    }

    @NotNull
    static public ArrayList<StudentProfileTableViewItem> genItems(@NotNull Student s) {
        ArrayList<StudentProfileTableViewItem> items = new ArrayList<>();
        items.add(new StudentProfileTableViewItem("Name", s.getFirstName() + " " + s.getLastName()));
        items.add(new StudentProfileTableViewItem("Username", s.getUsername()));
        items.add(new StudentProfileTableViewItem("Year", String.valueOf(s.getEntryDate().get(Calendar.YEAR))));
        items.add(new StudentProfileTableViewItem("Alma mater", s.getAlmaMater().getName()));
        items.add(new StudentProfileTableViewItem("Email", s.getEmail()));


        return items;
    }

    public String getKey() {
        return key.get();
    }

    public StringProperty keyProperty() {
        return key;
    }

    public void setKey(String key) {
        this.key.set(key);
    }

    public String getValue() {
        return value.get();
    }

    public StringProperty valueProperty() {
        return value;
    }

    public void setValue(String value) {
        this.value.set(value);
    }
}

