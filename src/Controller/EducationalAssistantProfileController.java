package Controller;

import Model.EducationalSystem.Student;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.IOException;
import java.util.Calendar;

public class EducationalAssistantProfileController {
    @FXML private TextField usernameTextField;
    @FXML private TextField passwordTextField;
    @FXML private TextField emailTextField;
    @FXML private TextField firstNameTextField;
    @FXML private TextField nationalIDTextField;
    @FXML private TextField lastNameTextField;
    @FXML private TextField phoneNumberTextField;
    @FXML private ComboBox<String> alamaComboBox;
    @FXML private ComboBox<String> genderComboBox;
    @FXML private DatePicker birthDateDatePicker;
    @FXML private TextArea addressTextArea;
    @FXML private Button createButton;
    @FXML private MenuItem logoutMenuItem;

    public EducationalAssistantProfileController() {



    }

    @FXML
    private void initialize() {

        alamaComboBox.getItems().add("MASTER");
        alamaComboBox.getItems().add("BSC");
        alamaComboBox.getItems().add("PHD");
        genderComboBox.getItems().add("MALE");
        genderComboBox.getItems().add("FEMALE");

        createButton.setOnAction(event -> {
            new Student(firstNameTextField.getText(),
                    lastNameTextField.getText(),
                    usernameTextField.getText(),
                    passwordTextField.getText(),
                    Calendar.getInstance(),
                    nationalIDTextField.getText(),
                    genderComboBox.getValue(),
                    emailTextField.getText(),
                    phoneNumberTextField.getText(),
                    Calendar.getInstance(),
                    addressTextArea.getText(),
                    alamaComboBox.getValue());

        });

        logoutMenuItem.setOnAction(event -> {
            try {
                new Login();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
