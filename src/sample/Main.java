package sample;

import Controller.Login;
import Model.EducationalSystem.EducationalAssistant;
import Model.EducationalSystem.Student;
import Model.Messeging.Message;
import javafx.application.Application;
import javafx.stage.Stage;

import java.util.Calendar;

public class Main extends Application {

    static private Stage stage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        {
            stage = primaryStage;
            new Login();
            //System.out.println(user.getFirstName());
            //mainMenu(user);
        }
    }

    public static void main(String[] args) {
        Student sam = new Student("sam", "asadi", "m", "m", Calendar.getInstance(),
                "955555", "MALE", "sam@as.com", "435", Calendar.getInstance(),
                "Garden", "PHD");

        Student ali = new Student("ali", "faraz", "a", "a", Calendar.getInstance(),
                "955555", "MALE", "a@y.com", "435", Calendar.getInstance(),
                "Garden", "MASTER");

        EducationalAssistant educationalAssistant = new EducationalAssistant("pars", "Eiyan",
                "p", "p", Calendar.getInstance(), "89898", "FEMALE");

        Message message = Message.sendMessage("Hello World", "1st test", sam, ali);

        launch(args);
    }

    public static Stage getStage() {
        return stage;
    }
}
