package Model.RuleManagment.Rules;

import Model.EducationalSystem.Course;
import Model.RuleManagment.Rule;
import Model.RuleManagment.RuleType;
import Model.SystemChart.Department;

import java.util.Calendar;

public class RuleExamDate extends Rule {
    private Calendar examDate;
    private Course course;

    public RuleExamDate(Department department, Course course,Calendar examDate) {
        super(RuleType.ExamDate, department);
        this.examDate = examDate;
        this.course = course;
    }
}
