package Model.Messeging;

import Model.Repo;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

public class Message {
    private String content;
    private String title;
    private Inbox recipientInbox;
    private Inbox originatorInbox;
    private LocalDateTime sentDateTime;

    private Message(String content, String title, Inbox originatorInbox, Inbox recipientInbox) {
        this.content = content;
        this.title = title;
        this.recipientInbox = recipientInbox;
        this.originatorInbox = originatorInbox;
        this.sentDateTime = LocalDateTime.now();

        Repo.getRepo().getMessages().add(this);
    }

    @NotNull
    static public Message sendMessage(String content, String title, @NotNull Party originator, @NotNull Party recipient) {
        Message m = new Message(content, title, originator.getInbox(), recipient.getInbox());
        originator.getInbox().getSent().add(m);
        recipient.getInbox().getReceived().add(m);
        return m;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Inbox getRecipientInbox() {
        return recipientInbox;
    }

    public void setRecipientInbox(Inbox recipientInbox) {
        this.recipientInbox = recipientInbox;
    }

    public Inbox getOriginatorInbox() {
        return originatorInbox;
    }

    public void setOriginatorInbox(Inbox originatorInbox) {
        this.originatorInbox = originatorInbox;
    }

    public LocalDateTime getSentDateTime() {
        return sentDateTime;
    }
}
