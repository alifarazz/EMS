package Model.Messeging;

import Model.Repo;

public interface Party {
    static Party getPartyByUsername(String username) {
        for (Party p : Repo.getRepo().getParties())
            if (p.getUsername().equals(username))
                return p;
        return null;
    }

    Inbox getInbox();
    void setInbox(Inbox inbox);
    String getUsername();
}
