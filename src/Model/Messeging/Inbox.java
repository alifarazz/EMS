package Model.Messeging;

import Model.Repo;

import java.util.ArrayList;

public class Inbox {
    private Party party;
    private ArrayList<Message> received = new ArrayList<>();
    private ArrayList<Message> sent = new ArrayList<>();

    public Inbox(Party party) {
        this.party = party;

        Repo.getRepo().getInboxes().add(this);
    }

    public Party getParty() {
        return party;
    }

    public void setParty(Party party) {
        this.party = party;
    }

    public ArrayList<Message> getReceived() {
        return received;
    }

    public void setReceived(ArrayList<Message> received) {
        this.received = received;
    }

    public ArrayList<Message> getSent() {
        return sent;
    }

    public void setSent(ArrayList<Message> sent) {
        this.sent = sent;
    }
}
