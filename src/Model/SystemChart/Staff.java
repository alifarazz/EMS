package Model.SystemChart;

import Model.Messeging.Inbox;
import Model.Messeging.Party;
import Model.UserManagment.User;
import org.jetbrains.annotations.NotNull;

import java.util.Calendar;

abstract public class Staff extends User implements Party {
    protected Inbox inbox = new Inbox(this);

    public Staff(String firstName, String lastName, String username, String password, Calendar birthdate, String nationalID, @NotNull String genderStr) {
        super(firstName, lastName, username, password, birthdate, nationalID, genderStr);

    }

    @Override
    public Inbox getInbox() {
        return inbox;
    }

    @Override
    public void setInbox(Inbox inbox) {
        this.inbox = inbox;
    }
}