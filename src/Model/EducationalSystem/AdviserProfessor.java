package Model.EducationalSystem;

import Model.EducationalSystem.Exceptions.ExceptionCourseEdit;
import Model.Repo;
import Model.SystemChart.Department;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;

public class AdviserProfessor extends Professor {
    public AdviserProfessor(String firstName, String lastName, String username, String password, Calendar birthdate, String nationalID, @NotNull String genderStr, ArrayList<Course> courses) {
        super(firstName, lastName, username, password, birthdate, nationalID, genderStr, courses);

        Repo.getRepo().getAdviserProfessors().add(this);
    }

    public void remvoeCourseFromDepartment(Course course, Department department) throws ExceptionCourseEdit {
        if (department.editCourseIsActive())
            department.removeCourse(course);
        else
            throw new ExceptionCourseEdit();
    }

    public void addCourse2Department(Course course, Department department) throws ExceptionCourseEdit {
        if (department.editCourseIsActive())
            department.addCourse(course);
        else
            throw new ExceptionCourseEdit();
    }

}
