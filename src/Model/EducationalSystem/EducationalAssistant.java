package Model.EducationalSystem;

import Model.EducationalSystem.Exceptions.ExceptionCourseFull;
import Model.Repo;
import Model.SystemChart.Staff;
import org.jetbrains.annotations.NotNull;

import java.util.Calendar;

public class EducationalAssistant extends Staff{
    public EducationalAssistant(String firstName, String lastName, String username, String password, Calendar birthdate, String nationalID, @NotNull String genderStr) {
        super(firstName, lastName, username, password, birthdate, nationalID, genderStr);

        Repo.getRepo().getEducationalAssistants().add(this);
    }

    public void addStudent2Course (Student student, Course course) throws ExceptionCourseFull{
        course.addStudent(student);
    }
    public void removeStudentFromCourse(Student student, Course course) {
        course.removeStudent(student);
    }

    public void assignProfessor2Course(Professor professor, Course course) {
        course.assignInstructor(professor);
    }

    public void removeProfessorFromCourse(Course course) {
        course.removeInstructor();
    }

}
