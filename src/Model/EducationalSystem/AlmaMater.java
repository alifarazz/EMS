package Model.EducationalSystem;

import org.jetbrains.annotations.NotNull;

public enum  AlmaMater{
    BSC("BACHELOR"),
    MSC("MASTER"),
    PHD("PHD");

    private String name;

    AlmaMater(String name){ this.name = name; }

    public String getName() {
        return this.name;
    }

    public void setName(@NotNull String name) {
        if (name.equalsIgnoreCase("BACHELOR") ||
                name.equalsIgnoreCase("MASTER") ||
                name.equalsIgnoreCase("PHD"))
            this.name = name;
    }
}
